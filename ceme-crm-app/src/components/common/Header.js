import React from "react";
// import { NavLink } from "react-router-dom";
import { Form, Button, Nav, Navbar, FormControl } from "react-bootstrap";

function Header() {
	// const activeStyle = { color: "orange" };
	return (
		<>
			<Navbar bg="dark" variant="dark">
				<Navbar.Brand href="/">CEME CRM</Navbar.Brand>
				<Nav className="mr-auto">
					<Nav.Link href="/">Home</Nav.Link>
					<Nav.Link href="addcustomer">Add Customer</Nav.Link>
					<Nav.Link href="aboutus">About Us</Nav.Link>
				</Nav>
				<Form inline>
					<FormControl type="text" placeholder="Search" className="mr-sm-2" />
					<Button variant="outline-info">Search</Button>
				</Form>
			</Navbar>
		</>
		// <nav>
		// 	<NavLink activeStyle={activeStyle} exact to="/">
		// 		Home
		// 	</NavLink>{" "}
		// 	|{" "}
		// 	<NavLink activeStyle={activeStyle} exact to="/addcustomer">
		// 		Add Customer
		// 	</NavLink>{" "}
		// 	|{" "}
		// 	<NavLink activeStyle={activeStyle} exact to="/aboutus">
		// 		About Us
		// 	</NavLink>{" "}
		// </nav>
	);
}

export default Header;
