import React, { useState, useEffect } from "react";
import { Form, Button, Col } from "react-bootstrap";
import PolicyModal from "./PolicyModal";

function AddCustomerForm() {
	const [validated, setValidated] = useState(false);

	const policy = {
		policyNumber: "",
		premium: "",
		effectiveDate: "",
		productType: "",
	};

	const customerData = {
		firstName: "",
		lastName: "",
		email: "",
		contactNumber: "",
		address: "",
		state: "",
		zipcode: "",
		policyListt: [],
	};

	const [policyList, setPolicyList] = useState();

	const [customer, setCustomer] = useState(customerData);

	useEffect(() => {
		//console.log(customer);
	}, [customer]);

	const handleSubmit = (event) => {
		const form = event.currentTarget;
		if (form.checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();
		}

		setValidated(true);
		console.log(customer);
	};

	const changeHandler = (event) => {
		setCustomer({ ...customer, [event.target.name]: event.target.value });
	};

	// const isValidName = (e) => {
	// 	if (e.target.value.match("^[a-zA-Z ]*$") != null) {
	// 		return true;
	// 	}
	// 	return false;
	// };

	return (
		<Form noValidate validated={validated} onSubmit={handleSubmit}>
			<Form.Row>
				<Form.Group as={Col} md="4" controlId="firstName" className="text-left">
					<Form.Label>First name</Form.Label>
					<Form.Control
						required
						type="text"
						name="firstName"
						placeholder="First name"
						value={customer.firstName}
						pattern="[a-zA-Z]*"
						minLength="5"
						maxLength="15"
						onChange={(e) => changeHandler(e)}
					/>
					<Form.Control.Feedback type="invalid">
						Please enter a valid first name
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="4" controlId="lastName" className="text-left">
					<Form.Label>Last name</Form.Label>
					<Form.Control
						required
						type="text"
						name="lastName"
						minLength="5"
						maxLength="15"
						placeholder="Last name"
						pattern="[a-zA-Z]*"
						onChange={(e) => changeHandler(e)}
					/>
					<Form.Control.Feedback type="invalid">
						Please enter a valid last name
					</Form.Control.Feedback>
				</Form.Group>
			</Form.Row>
			<Form.Row>
				<Form.Group as={Col} md="5" controlId="email" className="text-left">
					<Form.Label>Email address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						required
						name="email"
						value={customer.email}
						maxLength="50"
						onChange={(e) => changeHandler(e)}
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
					<Form.Control.Feedback type="invalid">
						Please enter a valid mail Id
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group
					as={Col}
					md="3"
					controlId="contactNumber"
					className="text-left"
				>
					<Form.Label>Contact Number</Form.Label>
					<Form.Control
						type="text"
						placeholder="Optional"
						pattern="[0-9]*"
						name="contactNumber"
						value={customer.contactNumber}
						onChange={(e) => changeHandler(e)}
						maxLength="10"
					/>
					<Form.Text className="text-muted">
						We'll never share your contact number with anyone else.
					</Form.Text>
					<Form.Control.Feedback type="invalid">
						Please enter a valid contact number
					</Form.Control.Feedback>
				</Form.Group>
			</Form.Row>
			<Form.Row>
				<Form.Group
					as={Col}
					md="8"
					controlId="addressLine1"
					className="text-left"
				>
					<Form.Label>Address 1</Form.Label>
					<Form.Control
						type="text"
						placeholder="house no, street address"
						required
						pattern="[a-zA-Z0-9]*"
						name="address"
						value={customer.address}
						onChange={(e) => changeHandler(e)}
						minLength="5"
						maxLength="25"
					/>
					<Form.Control.Feedback type="invalid">
						Please provide a valid address.
					</Form.Control.Feedback>
				</Form.Group>
			</Form.Row>
			<Form.Row>
				<Form.Group as={Col} md="3" controlId="city" className="text-left">
					<Form.Label>City</Form.Label>
					<Form.Control
						name="city"
						type="text"
						placeholder="City"
						required
						pattern="[a-zA-Z]*"
						value={customer.city}
						onChange={(e) => changeHandler(e)}
						minLength="3"
						maxLength="15"
					/>
					<Form.Control.Feedback type="invalid">
						Please provide a valid city.
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="3" controlId="city" className="text-left">
					<Form.Label>State</Form.Label>
					<Form.Control
						as="select"
						name="state"
						type="l"
						placeholder="State"
						required
						pattern="[a-zA-Z]"
						value={customer.state}
						onChange={(e) => changeHandler(e)}
					>
						<option value="">Select</option>
						<option value="AL">Alabama</option>
						<option value="AK">Alaska</option>
						<option value="AZ">Arizona</option>
						<option value="AR">Arkansas</option>
						<option value="CA">California</option>
						<option value="CO">Colorado</option>
						<option value="CT">Connecticut</option>
						<option value="DE">Delaware</option>
						<option value="DC">District Of Columbia</option>
						<option value="FL">Florida</option>
						<option value="GA">Georgia</option>
						<option value="HI">Hawaii</option>
						<option value="ID">Idaho</option>
						<option value="IL">Illinois</option>
						<option value="IN">Indiana</option>
						<option value="IA">Iowa</option>
						<option value="KS">Kansas</option>
						<option value="KY">Kentucky</option>
						<option value="LA">Louisiana</option>
						<option value="ME">Maine</option>
						<option value="MD">Maryland</option>
						<option value="MA">Massachusetts</option>
						<option value="MI">Michigan</option>
						<option value="MN">Minnesota</option>
						<option value="MS">Mississippi</option>
						<option value="MO">Missouri</option>
						<option value="MT">Montana</option>
						<option value="NE">Nebraska</option>
						<option value="NV">Nevada</option>
						<option value="NH">New Hampshire</option>
						<option value="NJ">New Jersey</option>
						<option value="NM">New Mexico</option>
						<option value="NY">New York</option>
						<option value="NC">North Carolina</option>
						<option value="ND">North Dakota</option>
						<option value="OH">Ohio</option>
						<option value="OK">Oklahoma</option>
						<option value="OR">Oregon</option>
						<option value="PA">Pennsylvania</option>
						<option value="RI">Rhode Island</option>
						<option value="SC">South Carolina</option>
						<option value="SD">South Dakota</option>
						<option value="TN">Tennessee</option>
						<option value="TX">Texas</option>
						<option value="UT">Utah</option>
						<option value="VT">Vermont</option>
						<option value="VA">Virginia</option>
						<option value="WA">Washington</option>
						<option value="WV">West Virginia</option>
						<option value="WI">Wisconsin</option>
						<option value="WY">Wyoming</option>
					</Form.Control>
					<Form.Control.Feedback type="invalid">
						Please select a valid state.
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="2" controlId="zipcode" className="text-left">
					<Form.Label>Zip</Form.Label>
					<Form.Control
						name="zipcode"
						type="text"
						placeholder="Zip"
						required
						pattern="[0-9]*"
						value={customer.zipcode}
						onChange={(e) => changeHandler(e)}
						minLength="5"
						maxLength="5"
					/>
					<Form.Control.Feedback type="invalid">
						Please provide a valid zip.
					</Form.Control.Feedback>
				</Form.Group>
			</Form.Row>
			<Form.Group className="text-left">
				<PolicyModal />
			</Form.Group>
			<Form.Group className="text-left">
				<Form.Check
					required
					label="Agree to terms and conditions"
					feedback="You must agree before submitting."
				/>
			</Form.Group>
			<Button type="submit" className="text-left">
				Save Customer
			</Button>
		</Form>
	);
}
function AddCustomerPage(props) {
	return (
		<div className="jumbotron">
			<AddCustomerForm />
		</div>
	);
}

export default AddCustomerPage;
