import React, { useEffect } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { getApplicationStatus } from "./../redux/actions/index";

function HomePage(props) {
	const dispatch = useDispatch();

	const statusInfo = useSelector((state) => state.status.status);
	useEffect(() => {
		dispatch(getApplicationStatus());
	});

	return (
		<div className="jumbotron">
			<div className="row">
				<h3>CEME CRM Application</h3>{" "}
			</div>
			<div className="row">
				<h4>App Status</h4>
			</div>
			<div className="row"></div>
			<div className="row">{statusInfo}</div>
		</div>
	);
}

const mapStateToProps = (state) => {
	return {
		status: state.status.status,
	};
};

const actionCreators = {
	getApplicationStatus,
};

export default connect(mapStateToProps, actionCreators)(HomePage);
