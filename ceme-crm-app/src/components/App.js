import "./App.css";
import Header from "./common/Header";
import { Switch, Route } from "react-router-dom";
import HomePage from "./HomePage";
import AboutPage from "./AboutUs";

import AddCustomerPage from "./AddCustomerPage";

function App() {
	return (
		<div className="App">
			<div className="container">
				<div className="row">
					<Header />
				</div>

				<Switch>
					<Route path="/" exact component={HomePage} />
					<Route path="/addcustomer" component={AddCustomerPage} />
					<Route path="/aboutus" component={AboutPage} />
				</Switch>
			</div>
		</div>
	);
}

export default App;
