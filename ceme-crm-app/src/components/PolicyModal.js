import React, { useState } from "react";

import {
	Form,
	Button,
	Nav,
	Navbar,
	FormControl,
	Col,
	Modal,
} from "react-bootstrap";
// import { useSelector } from "react-redux";

const PolicyModal = () => {
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	return (
		<>
			<Button variant="primary" onClick={handleShow}>
				Add Policy
			</Button>

			<Modal show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title>Policy Information</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Row>
						<Form.Group
							as={Col}
							md="6"
							controlId="policyNumber"
							className="text-left"
						>
							<Form.Label>Policy Number</Form.Label>
							<Form.Control
								required
								type="text"
								name="policyNumber"
								placeholder="Policy Number"
								value=""
								pattern="[0-9]*"
								minLength="5"
								maxLength="15"
								// onChange={(e) => changeHandler(e)}
							/>
							<Form.Control.Feedback type="invalid">
								Please enter a valid policy number
							</Form.Control.Feedback>
						</Form.Group>
						<Form.Group
							as={Col}
							md="4"
							controlId="premium"
							className="text-left"
						>
							<Form.Label>Premium</Form.Label>
							<Form.Control
								required
								type="text"
								name="premium"
								minLength="5"
								maxLength="5"
								placeholder="Amount"
								pattern="[0-9]*"
								// onChange={(e) => changeHandler(e)}
							/>
							<Form.Control.Feedback type="invalid">
								Please enter a valid amount
							</Form.Control.Feedback>
						</Form.Group>
					</Form.Row>
					<Form.Row>
						<Form.Group
							as={Col}
							md="6"
							controlId="effectiveDate"
							className="text-left"
						>
							<Form.Label>Select Date</Form.Label>
							<Form.Control
								type="date"
								name="effectiveDate"
								placeholder="Policy start date"
							/>

							<Form.Control.Feedback type="invalid">
								Please enter a valid date
							</Form.Control.Feedback>
						</Form.Group>
						<Form.Group
							as={Col}
							md="4"
							controlId="Product"
							className="text-left"
						>
							<Form.Label>Policy Type</Form.Label>
							<Form.Control
								as="select"
								name="product"
								type="l"
								placeholder="product"
								required
								pattern="[a-zA-Z]"
								// value={customer.state}
								// onChange={(e) => changeHandler(e)}
							>
								<option value="">Select</option>
								<option value="auto">Auto</option>
								<option value="home">Home</option>
								<option value="renters">Renters</option>
							</Form.Control>
							<Form.Control.Feedback type="invalid">
								Please select a valid product
							</Form.Control.Feedback>
						</Form.Group>
					</Form.Row>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={handleClose}>
						Close
					</Button>
					<Button variant="primary" onClick={handleClose}>
						Save Policy
					</Button>
				</Modal.Footer>
			</Modal>
		</>
	);
};

export default PolicyModal;
